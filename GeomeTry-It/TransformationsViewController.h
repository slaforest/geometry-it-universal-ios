//
//  TransformationsViewController.h
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomTabController;
@interface TransformationsViewController : UIViewController <UISplitViewControllerDelegate, UIWebViewDelegate>

@property (weak, nonatomic) CustomTabController* tabController;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end
