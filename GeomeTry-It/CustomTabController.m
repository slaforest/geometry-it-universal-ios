//
//  CustomTabController.m
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import "CustomTabController.h"

@interface CustomTabController ()

@end

@implementation CustomTabController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     [self setPortraitOK:YES];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setPortraitOK:(BOOL)isPortOK{
    if (isPortOK) {
        isPortraitOK = YES;
    }else{
        isPortraitOK = NO;
    }
}
-(NSUInteger)supportedInterfaceOrientations
{
    if (self.selectedViewController)
        return [self.selectedViewController supportedInterfaceOrientations];
    
    return UIInterfaceOrientationMaskAll;
}

-(BOOL)shouldAutorotate
{
    return YES;
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
}



@end
