//
//  CustomTabController.h
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTabController : UITabBarController
{  BOOL isPortraitOK;
}

-(void)setPortraitOK:(BOOL)isPortOK;
@end
