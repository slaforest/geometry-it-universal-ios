//
//  ProtractorViewController.h
//  GeomeTry-It
//
//  Created by Scott LaForest on 6/5/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ProtractorViewController : UIViewController <UIImagePickerControllerDelegate, UISplitViewControllerDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate>{
    BOOL newMedia;
    CGPoint ptA;
    CGPoint vertex;
    CGPoint ptB;
    int touchesCounter;
    NSMutableArray* touchesHolder;
    BOOL isConjugate;
}
@property (nonatomic, retain) IBOutlet UIImageView* photoView;
@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (nonatomic, strong) UIImagePickerController *picker;
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (nonatomic, retain) IBOutlet UILabel* degreeVal;
@property (nonatomic, retain) IBOutlet UILabel* radVal;
@property (nonatomic, retain) IBOutlet UIButton* conjBtn;

-(IBAction)conjugatePressed:(id)sender;

@end
