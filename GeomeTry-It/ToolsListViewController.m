//
//  ToolsListViewController.m
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import "ToolsListViewController.h"
#import "DetailViewController.h"
#import "TransformationsViewController.h"
#import "ProtractorViewController.h"
#import "AppDelegate.h"
#import "PythagoreanViewController.h"
#import "RightTriangleCheckerViewController.h"

@interface ToolsListViewController ()

@end

@implementation ToolsListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    toolsList = [[NSArray alloc]initWithObjects:@"3D Shapes", @"Transformations", @"Protractor",@"Pythagorean Theorem Solver", @"Right Triangle Checker",nil];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return toolsList.count  ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = toolsList[indexPath.row];
    cell.textLabel.textColor = [UIColor colorWithRed:0 green:0.533 blue:1.0 alpha:1.0];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    NSString* urlString = @"";
    if (indexPath.row == 0){
        urlString = @"http://www.geometry-it.appspot.com/3d_main";
    }else if (indexPath.row == 1){
        urlString = @"http://www.geometry-it.appspot.com/transformations";
    }
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
	    
            if (!self.detailViewController) {
	        self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPhone" bundle:nil];
            }
        
        if (indexPath.row == 2) {
            ProtractorViewController* protractorVC = [[ProtractorViewController alloc] initWithNibName:@"ProtractorViewController" bundle:nil];
            protractorVC.navigationItem.title = toolsList[indexPath.row];
            [self.navigationController pushViewController:protractorVC animated:YES];
        }else if (indexPath.row == 3){
            PythagoreanViewController* pvc = [[PythagoreanViewController alloc]initWithNibName:@"PythagoreanViewController" bundle:nil];
            pvc.navigationItem.title = toolsList[indexPath.row];
            [self.navigationController pushViewController:pvc animated:YES];
        }else if (indexPath.row == 4){
            RightTriangleCheckerViewController* pvc = [[RightTriangleCheckerViewController alloc]initWithNibName:@"RightTriangleCheckerViewController" bundle:nil];
            pvc.navigationItem.title = toolsList[indexPath.row];
            [self.navigationController pushViewController:pvc animated:YES];
            
        }

        else{
            self.detailViewController.navigationItem.title = toolsList[indexPath.row];
            [self.navigationController pushViewController:self.detailViewController animated:YES];
        }
    }
    else {
        AppDelegate* appDelgate = [[UIApplication sharedApplication]delegate];
        [appDelgate.splitViewController viewWillDisappear:YES];
        UIBarButtonItem* popoverButton = [[[[self.splitViewController.viewControllers objectAtIndex:1]topViewController]navigationItem ]leftBarButtonItem];
        NSMutableArray* viewControllerArray = [[NSMutableArray alloc]initWithArray:appDelgate.splitViewController.viewControllers];
        [viewControllerArray removeLastObject];
        
        
        if (indexPath.row == 0 || indexPath.row == 1) {
            DetailViewController* detailVC = [[DetailViewController alloc]initWithNibName:@"DetailViewController_iPad" bundle:nil];
            UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:detailVC];
            //detailNavigationController.navigationBar.tintColor = [UIColor blackColor];
            [viewControllerArray addObject:detailNavigationController];
            appDelgate.splitViewController.delegate = detailVC;
            detailVC.navigationItem.leftBarButtonItem = popoverButton;
            self.detailViewController = detailVC;
            detailVC.navigationItem.title =  toolsList[indexPath.row];
            detailVC.urlstring = urlString;
            
        }else if (indexPath.row == 2){
            ProtractorViewController* protractorVC = [[ProtractorViewController alloc]initWithNibName:@"ProtractorViewController_iPad" bundle:nil];
            UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:protractorVC];
            protractorVC.navigationItem.leftBarButtonItem = popoverButton;
            //detailNavigationController.navigationBar.tintColor = [UIColor blackColor];
            [viewControllerArray addObject:detailNavigationController];
            appDelgate.splitViewController.delegate= protractorVC;
            protractorVC.navigationItem.title = toolsList[indexPath.row];
        }else if (indexPath.row == 3){
            PythagoreanViewController* pvc = [[PythagoreanViewController alloc]initWithNibName:@"PythagoreanViewController_iPad" bundle:nil];
            UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:pvc];
            pvc.navigationItem.leftBarButtonItem = popoverButton;
            //detailNavigationController.navigationBar.tintColor = [UIColor blackColor];
            [viewControllerArray addObject:detailNavigationController];
            appDelgate.splitViewController.delegate= pvc;
            pvc.navigationItem.title = toolsList[indexPath.row];
        }
        else if (indexPath.row == 4){
            RightTriangleCheckerViewController* rtvc = [[RightTriangleCheckerViewController alloc]initWithNibName:@"RightTriangleCheckerController_iPad" bundle:nil];
            UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:rtvc];
            rtvc.navigationItem.leftBarButtonItem = popoverButton;
            //detailNavigationController.navigationBar.tintColor = [UIColor blackColor];
            [viewControllerArray addObject:detailNavigationController];
            appDelgate.splitViewController.delegate= rtvc;
            rtvc.navigationItem.title = toolsList[indexPath.row];
        }

        [appDelgate.splitViewController setViewControllers:viewControllerArray];
        [appDelgate.splitViewController viewWillAppear:YES];
    }
    
        NSURLRequest* request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [self.detailViewController.webView loadRequest:request];

}


@end
