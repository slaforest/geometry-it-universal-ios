//
//  AppDelegate.m
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import "AppDelegate.h"

#import "MasterViewController.h"
#import "ToolsListViewController.h"
#import "GlossaryListViewController.h"
#import "DetailViewController.h"
#import "CustomNavController.h"
#import "StoreViewController.h"
#import "FormulasViewController.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UINavigationController *navController1, *navController2;
    ToolsListViewController *toolsVc = [[ToolsListViewController alloc]initWithNibName:@"ToolsListViewController" bundle:nil];
    navController1 = [[UINavigationController alloc] initWithRootViewController:toolsVc];
    GlossaryListViewController *glossaryVC = [[GlossaryListViewController alloc]initWithNibName:@"GlossaryListViewController" bundle:nil];
    navController2 = [[UINavigationController alloc] initWithRootViewController:glossaryVC];
    toolsVc.navigationItem.title = NSLocalizedString(@"Tools", @"Tools");
    glossaryVC.navigationItem.title = NSLocalizedString(@"Glossary", @"Glossary");
    navController1.tabBarItem.title = NSLocalizedString(@"Tools", @"Tools");
    navController1.tabBarItem.image = [UIImage imageNamed:@"tools.png"];
    navController2.tabBarItem.image = [UIImage imageNamed:@"glossary.png"];
    navController2.tabBarItem.title = NSLocalizedString(@"Glossary", @"Glossary");
    //navController1.navigationBar.tintColor = [UIColor lightGrayColor];
    //navController2.navigationBar.tintColor = [UIColor lightGrayColor];
    
    
    FormulasViewController* functionsVC = [[FormulasViewController alloc]initWithNibName:@"GlossaryListViewController" bundle:nil];
    UINavigationController* functionsNav = [[UINavigationController alloc]initWithRootViewController:functionsVC];
    functionsVC.navigationItem.title = NSLocalizedString(@"Formulas", @"Formulas");
    functionsNav.tabBarItem.title = NSLocalizedString(@"Formulas", @"Formulas");
    functionsNav.tabBarItem.image = [UIImage imageNamed:@"formulas.png"];
        
    //functionsVC.tabBarItem.image = [UIImage imageNamed:@"pricetag.png"];
    

    NSDictionary* navTitleDict = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor magentaColor], UITextAttributeTextColor, [UIColor blackColor], UITextAttributeTextShadowColor,[NSValue valueWithUIOffset:UIOffsetMake(0, 1)],UITextAttributeTextShadowOffset, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:navTitleDict];
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.tabBar.tintColor = [UIColor darkGrayColor];
    
    self.tabBarController.viewControllers = @[navController1, navController2,functionsNav];
    
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        navController1.navigationBar.tintColor = [UIColor lightGrayColor];
        navController2.navigationBar.tintColor = [UIColor lightGrayColor];
        functionsNav.navigationBar.tintColor = [UIColor lightGrayColor];
        self.window.rootViewController = self.tabBarController;
    } else {
        
        
        DetailViewController *detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPad" bundle:nil];
        UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:detailViewController];
        //detailNavigationController.navigationBar.tintColor = [UIColor blackColor];

    	toolsVc.detailViewController = detailViewController;
    	glossaryVC.detailViewController = detailViewController;
        functionsVC.detailViewController = detailViewController;

        self.splitViewController = [[UISplitViewController alloc] init];
        self.splitViewController.delegate = detailViewController;
        self.splitViewController.viewControllers = @[self.tabBarController, detailNavigationController];
        
        self.window.rootViewController = self.splitViewController;
    }
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
