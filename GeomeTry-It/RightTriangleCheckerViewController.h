//
//  RightTriangleCheckerViewController.h
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
//#import "MasterViewController.h"

@class DetailViewController;

@interface RightTriangleCheckerViewController : UIViewController <UITextFieldDelegate, UISplitViewControllerDelegate>
{
    
    UITextField *activeField;
    
    double a;
	double b;
	double c;
    
    
}

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;

@property (nonatomic, retain) IBOutlet UITextField *aTextField;
@property (nonatomic, retain) IBOutlet UITextField *bTextField;
@property (nonatomic, retain) IBOutlet UITextField *cTextField;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UITextView *answerTextView;

@property (nonatomic, retain) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, retain)IBOutlet UIScrollView *scrollView;



-(IBAction)clearButtonPressed:(id)sender;
- (IBAction) checkButtonPressed: (id) sender;



@end
