//
//  GlossaryListViewController.m
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import "GlossaryListViewController.h"
#import "DetailViewController.h"
#import <CommonCrypto/CommonDigest.h>
#import "AppDelegate.h"
@interface GlossaryListViewController ()

@end

@implementation GlossaryListViewController
/*
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}*/

-(NSArray*) getAllGlossaryData:(NSString*)urlString{
    NSURL* url = [NSURL URLWithString:urlString];
    NSData* data = [NSData dataWithContentsOfURL:url];
    if (data) {
     
        NSError* error;
        NSArray* json = [NSJSONSerialization
                     JSONObjectWithData:data //1
                     
                     options:kNilOptions
                     error:&error];
        lastJSONGet = [NSDate date];
        return json;
    }else{
        UIAlertView* jsonError = [[UIAlertView alloc]initWithTitle:@"Couldn't access glossary database!" message:@"Check to make sure you have internet access or try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [jsonError show];
        return nil;
    }
   
}

-(NSArray*) getGlossaryTerms{
    NSMutableArray* tempArray = [[NSMutableArray alloc]init ];
    
    for (int i = 0; i<jsonArray.count;i++){
        NSString* currentTerm = [jsonArray[i] objectForKey:@"term"];
        NSString* currentDef = [jsonArray[i] objectForKey:@"definition"];
        currentTerm = [currentTerm capitalizedString];
         NSDictionary* currentDict = [[NSDictionary alloc]initWithObjectsAndKeys:currentTerm,@"term", currentDef,@"definition", nil];
        [tempArray addObject:currentDict];
    }
    
    NSArray *termsArray = [[NSArray alloc] initWithArray:tempArray];
    return termsArray;
}
-(NSArray*)getLettersNeeded:(NSArray*) arrOfTerms{
    NSMutableArray* tempLets =[[NSMutableArray alloc]init];
    for (int i = 0; i < arrOfTerms.count; i++){
        NSDictionary* myDict= [[NSDictionary alloc]initWithDictionary: arrOfTerms[i]];
        NSString* currStr = [myDict objectForKey:@"term"];
        NSLog(@"currSTR : %@", currStr);
        NSString* firstLetter = [currStr substringToIndex:1];
        NSLog(@"1st: %@ ", firstLetter);
        if (tempLets.count == 0) {
            [tempLets addObject:firstLetter];
        }
        else{
            for (int j = 0; j<tempLets.count; j++) {
                if (![tempLets containsObject:firstLetter]) {
                    [tempLets addObject:firstLetter];
                }
            }
        }
    
    }
    NSArray* letters = [[NSArray alloc]initWithArray:tempLets];
    return letters;
}

-(NSArray*) arrangeTermsInSections:(NSArray*)lettersList :(NSArray*)terms{
    //NSMutableDictionary* mutDict = [[NSMutableDictionary alloc]init];
    NSMutableArray* mutArray = [[NSMutableArray alloc]init];
    NSMutableArray* currTerms = [[NSMutableArray alloc]initWithArray:terms];
        for (int i = 0 ; i < lettersList.count; i++) {
            NSString* currentLetter = lettersList[i];
            NSMutableArray* currentTermsInSection = [[NSMutableArray alloc]init];
            for (int k = 0; k < currTerms.count; k++){
                
                NSString* firstLetter = [[currTerms[k] objectForKey:@"term" ] substringToIndex:1];
               
                if ([firstLetter isEqualToString:currentLetter]) {
                    [currentTermsInSection addObject:currTerms[k] ];
                    
                }
            }
   
        [mutArray addObject:currentTermsInSection];
        //[mutDict setValue:currentTermsInSection forKey:currentLetter];
        }
return mutArray;
}

-(void) viewWillAppear:(BOOL)animated{
    NSDate* now = [NSDate date];
    if ([now timeIntervalSinceDate:lastJSONGet] > 86400.0f || termsList.count < 1) {
        
        jsonArray = [self getAllGlossaryData:@"http://www.geometry-it.appspot.com/glossary"];
        termsList = [self getGlossaryTerms];
        lettersNeeded = [self getLettersNeeded:termsList];
        arrangedTerms = [self arrangeTermsInSections:lettersNeeded:termsList];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    /*_searchBar = [[UISearchBar alloc] initWithFrame: CGRectMake(0, 0, 320, 45)];
   / _searchBar.delegate = self;
    if (_searchBar != nil) {
        // Your UISearchBar specifics here
        self.tableView.tableHeaderView = _searchBar;
    }*/
    
    _searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    _searchBar.delegate = self;
    //termsList = [[NSArray alloc]initWithObjects:@"Acute Angle", @"Obtuse Angle",@"Right Angle", nil];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return isFiltered ? 1 :lettersNeeded.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray* myArray = arrangedTerms[section];
    // Return the number of rows in the section.
    return isFiltered ? searchedData.count :myArray.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return isFiltered ? nil:  lettersNeeded[section];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([self tableView:tableView titleForHeaderInSection:section] != nil) {
        return 35;
    }
    else {
        // If no section header title, no section header needed
        return 0;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label = [[UILabel alloc] init] ;
    label.frame = CGRectMake(20, 6, 300, 30);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor magentaColor];
    label.shadowColor = [UIColor blackColor];
    label.shadowOffset = CGSizeMake(0.0, 1.0);
    label.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:22];
    label.text = sectionTitle;
    
    // Create header view and add label as a subview
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
    
    [view addSubview:label];
    
    return isFiltered ? nil: view;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    NSArray* myArray = arrangedTerms[indexPath.section];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.textColor = [UIColor colorWithRed:0 green:0.533 blue:1.0 alpha:1.0];
    cell.textLabel.text = isFiltered ? [searchedData[indexPath.row]objectForKey:@"term"]: [myArray[indexPath.row] objectForKey:@"term"];
    return cell;
}

#pragma mark - SearchBar Delegate -
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if (searchText.length == 0)
        isFiltered = NO;
    else
        isFiltered = YES;
    
    NSMutableArray *tmpSearched = [[NSMutableArray alloc] init];
    NSMutableArray *tmpDict = [[NSMutableArray alloc] init];
    NSMutableArray *justTheTerms = [[NSMutableArray alloc]init ];
    for (int i = 0;i<termsList.count; i++) {
        [justTheTerms addObject:[termsList[i] objectForKey:@"term"]];
    }
    for (NSString* string in justTheTerms) {
        
        //we are going for case insensitive search here
        NSRange range = [string rangeOfString:searchText
                                      options:NSCaseInsensitiveSearch];
        
        if (range.location != NSNotFound && ![tmpSearched containsObject:string])
            [tmpSearched addObject:string];
    }
    for (int k = 0;k<justTheTerms.count; k++) {
        for (int l = 0; l <tmpSearched.count; l++) {
            if ([justTheTerms[k] isEqualToString:tmpSearched[l]]) {
                [tmpDict addObject:termsList[k]];
            }
        }
        
        
    }
    searchedData = tmpDict.copy;
    
    [self.tableView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
}
-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    isFiltered = NO;
    [self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    NSArray* myArray = arrangedTerms[indexPath.section];

    NSString* definition = isFiltered ? [searchedData[indexPath.row]objectForKey:@"definition"]: [myArray[indexPath.row] objectForKey:@"definition"];
    NSString* term = isFiltered ? [searchedData[indexPath.row]objectForKey:@"term"]: [myArray[indexPath.row] objectForKey:@"term"];
    
    NSLog(@"def: %@", definition);
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
	    if (!self.detailViewController) {
	        self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPhone" bundle:nil];
	    }
	    self.detailViewController.navigationItem.title = term;
        UILabel* tlabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0, 200, 40)];
        tlabel.text=self.detailViewController.navigationItem.title;
        tlabel.textColor=[UIColor magentaColor];
        tlabel.font = [UIFont fontWithName:@"Helvetica-Bold" size: 30.0];
        tlabel.backgroundColor =[UIColor clearColor];
        tlabel.adjustsFontSizeToFitWidth=YES;
        tlabel.textAlignment = NSTextAlignmentCenter;
    
        self.detailViewController.navigationItem.titleView = tlabel;
        [self.navigationController pushViewController:self.detailViewController animated:YES];
        [self.detailViewController.webView loadHTMLString:definition baseURL:nil];
    } else {
        AppDelegate* appDelgate = [[UIApplication sharedApplication]delegate];
        [appDelgate.splitViewController viewWillDisappear:YES];
        UIBarButtonItem* popoverButton = [[[[self.splitViewController.viewControllers objectAtIndex:1]topViewController]navigationItem ]leftBarButtonItem];
        NSMutableArray* viewControllerArray = [[NSMutableArray alloc]initWithArray:appDelgate.splitViewController.viewControllers];
        [viewControllerArray removeLastObject];
        
        
        DetailViewController* detailVC = [[DetailViewController alloc]initWithNibName:@"DetailViewController_iPad" bundle:nil];
        UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:detailVC];
        //detailNavigationController.navigationBar.tintColor = [UIColor blackColor];
        [viewControllerArray addObject:detailNavigationController];
        appDelgate.splitViewController.delegate = detailVC;
        detailVC.navigationItem.leftBarButtonItem = popoverButton;
        self.detailViewController = detailVC;
        self.detailViewController.definition = definition;
        
        
        [appDelgate.splitViewController setViewControllers:viewControllerArray];
        [appDelgate.splitViewController viewWillAppear:YES];
        self.detailViewController.navigationItem.title =  term;
        
        [self.detailViewController.webView loadHTMLString:definition baseURL:nil];
        
    }
}

@end
