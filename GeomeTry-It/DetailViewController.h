//
//  DetailViewController.h
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate, UIWebViewDelegate, UIAlertViewDelegate>
{
    
}

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property(nonatomic, strong) NSString* urlstring;
@property(nonatomic, strong) NSString* definition;
-(void) updateWebview;

@end
