//
//  GlossaryListViewController.h
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;
@interface GlossaryListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate, UIAlertViewDelegate>{
    NSArray *termsList;
    NSArray *jsonArray;
    NSArray* lettersNeeded;
    NSArray* arrangedTerms;
    NSArray *searchedData;
    NSDate* lastJSONGet;
    BOOL isFiltered;
    
}

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property(strong , nonatomic) IBOutlet UISearchBar* searchBar;
@end
